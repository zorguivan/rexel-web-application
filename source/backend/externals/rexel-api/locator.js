import axios from 'axios';

export default function generatelocator(target, destinations, res) {

    let origin = `https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&key=AIzaSyAvZjiyJbeq5JL7UX4zKcc478ayQAY4Svc&origins=${target.yextDisplayLat},${target.yextDisplayLng}&destinations=`;
    let locatorUrl;
    let targetUrls = [];
    let additions = 0;
    let splittedTarget = chunkArray(destinations, 100);
    let data = [];
    splittedTarget.forEach((dataChunck) => {
        locatorUrl = origin;
        dataChunck.forEach((destination) => {
            if (destination.id != target.id) {
                if (additions < 1) {
                    locatorUrl += destination.yextDisplayLat + '%2C' + destination.yextDisplayLng;
                } else {
                    locatorUrl += '%7C' + destination.yextDisplayLat + '%2C' + destination.yextDisplayLng;
                }
            }
            additions += 1;
        });
        targetUrls.push(locatorUrl);
    });

    let chain = targetUrls.map((url) => {
        let container = execute(url);
        return container;
    })

    Promise.all(chain).then((response) => {
        res.send(calculateDistance(data, destinations))
    }).catch((err) => {
        console.log(err)
        res.json({ error: "Something went wrong" })
    });

    function execute(locatorUrl) {
        return new Promise((resolve, reject) => {
            axios.get(locatorUrl).then((response) => {
                data.push(...response.data.rows[0].elements);
                resolve(response.rows);
            }).catch((response) => {
                reject(response);
                res.status(500).json({
                    message: 'Something went wrong. Try again later'
                })
            })
        })
    }
}

function chunkArray(myArray, chunk_size) {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];

    for (index = 0; index < arrayLength; index += chunk_size) {
        let chunk = myArray.slice(index, index + chunk_size);
        // Do something if you want with the group
        tempArray.push(chunk);
    }
    return tempArray;
}

function calculateDistance(data, destinations) {
    data.forEach((item, index) => {
        item.branchId = destinations[index].id;
    });

    data.sort((a, b) => { if (a.distance.value > b.distance.value) { return 1 } else { return -1 } });

    return [
        { b_id: data[0].branchId, distance: data[0].distance.value },
        { b_id: data[1].branchId, distance: data[1].distance.value },
        { b_id: data[2].branchId, distance: data[2].distance.value }
    ]
}
