import axios from 'axios';
import fs from 'fs';
import locator from './locator.js';



export default function attach(app) {
    app.get('/api/rexel/branchs/:id', (req, res, next) => {
        fs.readFile('./locations.json', 'utf8', function readFileCallback(err, data) {
            if (err) {
                console.log(err);
            } else {
                data = JSON.parse(data);
                if (data.locations) {
                    data.locations.forEach((location) => {
                        if (location.id == req.params.id) {
                            res.json({ data: location });
                        }
                    })
                } else {
                    res.json({ Error: "Not Found" })
                }
            }
        });
    });

    app.get('/api/rexel/store/branchs', (req, res, next) => {
        let branchData = [];
        let offsets = [0, 50, 100, 150, 200, 250, 300];
        let chain = offsets.map((offset) => {
            let container = execute(offset);
            return container;
        });
        Promise.all(chain).then((response) => {
            let data = { dataSize: branchData.length, locations: branchData }
            let json = JSON.stringify(data);
            fs.writeFile('./locations.json', json, 'utf8', (err, res) => {
                console.log('File Saved')
            });
            res.json(`Total of ${branchData.length} Branchs Received!`);
        }).catch((err) => {
            console.log(err)
            res.json({ error: "Something went wrong" })
        });

        function execute(offset) {
            return new Promise((resolve, reject) => {
                axios.get(`https://api.yext.com/v2/accounts/1713835/locationsearch?api_key=687eab4917c6d97ccad9dc5d64e77afb&v=20181012&limit=50&offset=${offset}`).then((response) => {
                    let data = response.data.response.locations;
                    branchData.push(...data);
                    resolve();
                }).catch((err) => {
                    reject(err)
                });
            })
        }
    });

    app.get('/api/rexel/distance/:id', (req, res, next) => {
        fs.readFile('./locations.json', 'utf8', function readFileCallback(err, data) {
            if (err) {
                console.log(err);
            } else {
                data = JSON.parse(data);
                if (data.locations) {
                    data.locations.forEach((location) => {
                        if (location.id == req.params.id) {
                            locator(location, data.locations, res, true);
                        }
                    })
                }
            }
        });
    });

    app.get('/api/rexel/distance/:lat/:lang', (req, res, next) => {
        // console.log(req.params.lat, req.params.lang);
        fs.readFile('./locations.json', 'utf8', function readFileCallback(err, data) {
            if (err) {
                console.log(err);
            } else {
                data = JSON.parse(data);
                if (data.locations) {
                    locator({ yextDisplayLat: req.params.lat, yextDisplayLng: req.params.lang }, data.locations, res);
                }
            }
        });
    });


    app.get('/api/rexel/brancheslist', (req, res, next) => {
        fs.readFile('./locations.json', 'utf8', function readFileCallback(err, data) {
            let branches = {}
            if (err) {
                console.log(err);
            } else {
                data = JSON.parse(data);
                if (data.locations) {
                    data.locations.forEach((branch) => {
                        if (branches[branch.city] > 0) {
                            branches[branch.city] += 1;
                        } else {
                            branches[branch.city] = 1;
                        }
                    });
                    let result = Object.keys(branches).map(function (key) {
                        return [key, branches[key]];
                    });
                    res.json({ length: result.length, data: result.sort()})
                }
            }
        });
    });

    app.get('/api/rexel/brancheslist/:city', (req, res, next) => {
        fs.readFile('./locations.json', 'utf8', function readFileCallback(err, data) {
            let branches = [];
            if (err) {
                console.log(err);
            } else {
                data = JSON.parse(data);
                if (data.locations) {
                    data.locations.forEach((branch) => {
                        if (branch.city == req.params.city) {
                            console.log('Triggered')
                            branches.push(branch)
                        }
                    });
                    res.json({ length: branches.length, data: branches })
                }
            }
        });
    })
}

