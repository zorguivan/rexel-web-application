import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import path from 'path';
import session from 'express-session';

//This is where the express app is created.
const app = express();

let context = {};

//This will ensure that all the static assets fetched are compressed, in order to make it faster to send, and make the files smaller.
app.use(compression());

//Initiate session, together with a key to encrypt all the session data.
app.use(session({
    secret: 'DeKatKrabtDeKrullenVanDeTrap'
}));

//Make all the static files directly accesable, these are all the files you put in the folder public. Be carefull because all files that you put here will be exposed to everyone who know the URL.
app.use('/', express.static('public'));

//Makes sure that you can POST or PUT Json data.
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));

//TODO: Here is where you put all your API middleware.
import attackRexel from './externals/rexel-api';
attackRexel(app);

//Catch all route, this is meant for if the request doesn't hit any of the above, this will be triggered, it's set to return index.html, this will make sure that react will be allowed to handle it on the client side.
app.all('/*', function(req, res) {
    res.sendFile('public/index.html');
});

//Finally after all has been configured, you active the app by listening on a certain port. 
//Port 80 is for http traffic
//Port 443 is for https trafic
//Port 8080 is for development http traffic
//Port 8443 is for development https traffic
const port = 8080;
app.listen(port, () => {
    console.log("App now listening on port " + port);
});