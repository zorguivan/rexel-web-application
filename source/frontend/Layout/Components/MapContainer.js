import React from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
 

const style = {
  width: '100%',
  height: '100%',
  padding: '0px'
}

export class MapContainer extends React.Component {
  onMarkerClick(){
    console.log('Hello')
  }
  render() {
    return (
      <Map 
      google={this.props.google} 
      style={style} 
      zoom={5}
      center={{
        lat: 51.7027384,
        lng: -1.9071438
      }}
      >
 
        <Marker onClick={this.onMarkerClick}
                name={'Current location'} />
 
        <InfoWindow onClose={this.onInfoWindowClose}>
            <div>
              <h1>Location</h1>
            </div>
        </InfoWindow>
      </Map>
    );
  }
}
 
export default GoogleApiWrapper({
  apiKey: ('AIzaSyAvZjiyJbeq5JL7UX4zKcc478ayQAY4Svc')
})(MapContainer)