import React from 'react';
import '../Style/Default.scss';
import {GoogleApiWrapper} from 'google-maps-react';
import MapContainer from './MapContainer';

class Home extends React.Component {
    render() {
        return (
            <div>
            <div id="mapContainer" ><MapContainer /></div>
            </div>
        );
    }
}

export default Home;