import React from 'react';
import './Style/Default.scss';
import { Route, Switch } from 'react-router';

import Home from './Components/Home';

class DefaultLayout extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" render={() => {
                    return <Home/>
                }} />
                <Route path="/:pagename" render={(props) => {
                    return <h1>Page requested is {props.match.params.pagename}</h1>
                }} />
            </Switch>
        );
    }
}

export default DefaultLayout;